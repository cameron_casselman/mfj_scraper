package mfjScrapperApplication.scraper

import org.jsoup.Jsoup
import java.lang.Exception
import java.util.jar.Attributes

class Scraper () {

    private val wikipediaUrl = "https://en.wikipedia.org"
    /*
    * Returns a map state name and href="" for each state in USA
    */
    fun stateScraper(): List<Pair<String,String>> {
        val stateWikiUrl = "${wikipediaUrl}/wiki/U.S._state"
        val stateWikiCssQuery = ".div-col li a"
        var stateNameLinkMap = emptyList<Pair<String,String>>()
        try {
            Jsoup.connect(stateWikiUrl).get()
                .select(stateWikiCssQuery)
                .forEach {

                    stateNameLinkMap += when {
                        //Hawaii, Montana, and South Dakota do not list population data pertaining to cities on their main page
                        it.html() == "Hawaii" -> Pair(it.html(), "/wiki/List_of_places_in_Hawaii")
                        it.html() == "Montana" -> Pair(it.html(), "/wiki/List_of_municipalities_in_Montana")
                        it.html() == "South Dakota" -> Pair(it.html(), "/wiki/List_of_cities_in_South_Dakota")
                        else -> Pair(it.html(), it.attr("href"))
                    }
                }
        } catch (e: Exception) {
            println("Error while connecting to url $e")
            return stateNameLinkMap
        }
        return stateNameLinkMap
    }

    // I don't think this is the most efficient way to do this.
    // I query for table:nth-child(x).... What if that changes ? sometimes its the 7th, 8th or 6th child depending on how the table is formatted
    // Is there a better way to do this?
    // Now that I think about it, there's probably a way to select the css class instead of the :nth-child, but for sake of time, going to stick with nth-child
    // Let me know if there's a better way. I am new to css selectors and Jsoup :)
    // Take a look at the areaCodeScraper to see a better way of implementing a function like this.

    //I wanted to query for css selector table:wikitable tr td a, and search for td = 6... but what if there's another table with a td = 6? How to differentiate
    //without manually checking to see where the child is?

    fun cityScraper(stateNameHrefPair: Pair<String,String>): Pair<String, String> {
        var stateCityNameLinkPair = Pair<String,String>("","")
        var cssQuery = when(stateNameHrefPair.first) {
            "Alabama","New Jersey" -> "#mw-content-text > div.mw-parser-output > table:nth-child(129) > tbody > tr:nth-child(7) > td:nth-child(2) > a"
            "Alaska" -> "#mw-content-text > div.mw-parser-output > table:nth-child(258) > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(7) > td:nth-child(2) > a"
            //Colorado is an odd ball.. had to parse down to the 6th element in an ordered list
            "Colorado" -> "#mw-content-text > div.mw-parser-output > div:nth-child(335) > table > tbody > tr:nth-child(2) > td > div > div:nth-child(3) > table > tbody > tr:nth-child(2) > td.navbox-list.navbox-odd > div > div > ol > li:nth-child(6) > a"
            "Connecticut" -> "#mw-content-text > div.mw-parser-output > table:nth-child(147) > tbody > tr:nth-child(7) > td:nth-child(1) > a"
            "Hawaii" -> "#mw-content-text > div.mw-parser-output > table > tbody > tr:nth-child(6) > td:nth-child(2) > a"
            "Idaho" ->  "#mw-content-text > div.mw-parser-output > div.excerpt-block > div.excerpt > table > tbody > tr:nth-child(6) > td:nth-child(2) > a"
            "Illinois" -> "#mw-content-text > div.mw-parser-output > table:nth-child(111) > tbody > tr:nth-child(8) > td:nth-child(2) > a"
            "Iowa" -> "#mw-content-text > div.mw-parser-output > table:nth-child(107) > tbody > tr:nth-child(6) > td:nth-child(2) > a"
            "Kansas" -> "#mw-content-text > div.mw-parser-output > table:nth-child(87) > tbody > tr:nth-child(6) > td:nth-child(2) > a"
            "Maine" -> "#mw-content-text > div.mw-parser-output > table:nth-child(163) > tbody > tr:nth-child(1) > td:nth-child(6) > a"
            "Michigan" -> "#mw-content-text > div.mw-parser-output > table.navbox > tbody > tr:nth-child(8) > td:nth-child(2) > a"
            "Minnesota" -> "#mw-content-text > div.mw-parser-output > p:nth-child(63) > a:nth-child(6)"
            "Mississippi" -> "#mw-content-text > div.mw-parser-output > div:nth-child(35) > div > ol > li:nth-child(11) > a"
            "Montana" -> "#mw-content-text > div.mw-parser-output > table > tbody > tr:nth-child(6) > td:nth-child(1) > a"
            "Missouri" -> "#mw-content-text > div.mw-parser-output > table.navbox > tbody > tr:nth-child(8) > td:nth-child(2) > a"
            "Nebraska" -> "#mw-content-text > div.mw-parser-output > div.div-col > ul > li:nth-child(4) > a"
            "New Hampshire" -> "#mw-content-text > div.mw-parser-output > div:nth-child(223) > table > tbody > tr:nth-child(6) > td > div > ul > li:nth-child(12) > a"
            "North Dakota" -> "#mw-content-text > div.mw-parser-output > table:nth-child(193) > tbody > tr:nth-child(6) > td:nth-child(1) > a"
            "Rhode Island" -> "#mw-content-text > div.mw-parser-output > ul:nth-child(121) > li:nth-child(7) > div > div.gallerytext > p > a"
            "South Dakota" -> "#mw-content-text > div.mw-parser-output > table > tbody > tr:nth-child(6) > td:nth-child(2) > a"
            "Utah" -> "#mw-content-text > div.mw-parser-output > table:nth-child(243) > tbody > tr:nth-child(7) > td:nth-child(2) > a"
            "Vermont" -> "#mw-content-text > div.mw-parser-output > table:nth-child(29) > tbody > tr:nth-child(7) > th > a"
            "Wisconsin" -> "#mw-content-text > div.mw-parser-output > table:nth-child(181) > tbody > tr:nth-child(8) > td:nth-child(2) > a"
            "Wyoming" -> "#mw-content-text > div.mw-parser-output > table:nth-child(94) > tbody > tr:nth-child(7) > td:nth-child(2) > a"
            //This query works for most of of the states
            else -> "#mw-content-text > div.mw-parser-output > table.navbox > tbody > tr:nth-child(8) > td:nth-child(2) > a"
        }
        try{
            Jsoup.connect("$wikipediaUrl${stateNameHrefPair.second}").get()
                .select(cssQuery)
                .forEach {
                    stateCityNameLinkPair = when(stateNameHrefPair.first) {
                        //Virginia and Nebraska do not list area codes
                        "Virginia" -> Pair(it.attr("title").substringBefore(","), "/wiki/Charlottesville,_Virginia")
                        else -> Pair(it.attr("title").substringBefore(","), it.attr("href"))
                    }
                }
        } catch (e: Exception){
            return stateCityNameLinkPair
        }
        return stateCityNameLinkPair
    }

    /*
    * Scrapes the area codes associated with the city
    * */
    fun areaCodesScraper(stateCityHref: Pair<String,String>): List<String> {
        var cityAreaCodes = emptyList<String>()
        val cssQuery = when(stateCityHref.first) {
            //Fermont was the only odd ball here. It did not contain an area code title property.
            // I technically could have checked the parent elements first child which contained "Area Code HTML text" but for sake of time not doing this.
            "Fremont" -> "table.infobox.geography.vcard tr:nth-child(25) > td"
            else ->  "table.infobox.geography.vcard tr td a"
        }
        try{
            Jsoup.connect("$wikipediaUrl${stateCityHref.second}").get()
                .select(cssQuery)
                .forEach {
                    when {
                        it.attr("title").contains("Area Code",true) -> cityAreaCodes = areaCodeSplitter(it.html())
                        stateCityHref.first == "Fremont" -> cityAreaCodes = areaCodeSplitter(it.html());
                    }
                }
        } catch (e: Exception){
            return cityAreaCodes
        }
        return cityAreaCodes
    }

   private fun areaCodeSplitter(areaCodes: String): List<String> {
       return areaCodes.split("/", "and", "&amp;").map { x ->
           x.replace("\\s\\d".toRegex(), "")
       }
   }
}