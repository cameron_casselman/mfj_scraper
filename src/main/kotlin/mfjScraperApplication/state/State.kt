package mfjScraperApplication.state

data class State(val name: String, val sixthLargestCity: Pair<String, String>, val areaCodes: List<String>) {

    override fun toString(): String {
        return "$name's 6th largest city is ${sixthLargestCity.first} with area codes ${areaCodes.joinToString().replace("\\s".toRegex(), "")}"
    }
}