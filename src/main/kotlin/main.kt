import mfjScraperApplication.state.State
import mfjScrapperApplication.scraper.Scraper

fun main() {

    val scraper = Scraper()

    //Grab state names and their wiki pages
    val stateUrlPair = scraper.stateScraper()

    var stateCityUrlPairList = emptyList<Pair<String,String>>()
    //grab city name and their wiki pages
    stateUrlPair.forEach { stateCityUrlPairList += scraper.cityScraper(it) }

    //Grab the cities area codes
    var cityAreaCodeList = mutableListOf<List<String>>()
    stateCityUrlPairList.forEach { cityAreaCodeList.add(scraper.areaCodesScraper(it))}

    val listOfCitiesAndAreaCodes = mutableListOf<Any>()
    //Store all information in an object to print nicely
    if(cityAreaCodeList.size == 50 && stateCityUrlPairList.size == 50 && stateUrlPair.size == 50) {
        stateUrlPair.forEachIndexed{ index, element ->
            val state = State(element.first, stateCityUrlPairList[index], cityAreaCodeList[index])
            listOfCitiesAndAreaCodes += state
        }
    }
    listOfCitiesAndAreaCodes.forEach(::println)
}

